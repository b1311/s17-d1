// // Create array

// const array1 = ['eat', 'sleep'];
// console.log(array1);

// //use the new keyword
// const array2 = new Array('pray', 'play');
// console.log(array2);

// //empty array
// const myList = [];

// // array of numbers
// const numArray = [2, 3, 4, 5];

// //array of strings
// const stringArray = ['eat', 'work', 'pray', 'play'];

// //array of mixed

// const newData = ['work', 1, true];

// const newData1 = [
// 	{'task1': 'exercise'},
// 	[1, 2, 3],
// 	function hello(){
// 			console.log('Hi I am array.')
// 	}
// ];
// console.log(newData1);


// // Create an array with 7 items; all strings.
// 	//- list seven places you want to visit

// const placesArray = ['France', 'Australia', 'New Zealand', 'USA', 'Japan', 'Iceland', 'Norway'];
// console.log(placesArray[0]);	
// console.log(placesArray);
// console.log(placesArray[placesArray.length - 1]);
// console.log(placesArray.length);

// for(let i = 0; i < placesArray.length; i++){
// 	console.log(placesArray[i]);
// }

// //Array Manipulation
// //Add element to an array - push() ad delement at the end of the array

// let dailyActivities = ['eat', 'work', 'pray', 'play'];
// dailyActivities.push('exercise');
// console.log(dailyActivities);

// //unshift() add element at the beginning of the array
// dailyActivities.unshift('sleep');
// console.log(dailyActivities);


// dailyActivities[2] = 'sing';
// console.log(dailyActivities);


// dailyActivities[6] = 'dance';
// console.log(dailyActivities);


// //Re-assign the values/items in an array;
// placesArray[3] = 'Giza Sphinx';
// console.log(placesArray)
// console.log(placesArray[5]);

// placesArray[5] = 'Turkey';
// console.log(placesArray)

// let placesToVisit = ['France', 'Australia', 'New Zealand', 'USA', 'Japan', 'Iceland', 'Norway'];
// console.log(placesToVisit);

// placesToVisit[0] = 'Cavite City';
// placesToVisit[placesToVisit.length-1] = 'SSCR';
// console.log(placesToVisit);
// console.log(placesToVisit[0]);
// console.log(placesToVisit[placesToVisit.length-1]);
// console.log(placesToVisit[6]);

// // ADding items in an array without using methods
// let array = [];
// console.log(array[0]);
// array[0] = 'Cloud Strife';
// console.log(array);

// console.log(array[1]);
// array[1] = 'Tifa Lockhart';
// console.log(array[1]);
// array[array.length-1] = 'Aerith Gains';
// console.log(array);
// array[array.length] = 'Vincent Valentine';
// console.log(array);

// // Array Methods
// 	//Manipulate array with pre-determined JS Functions
// 	// Mutators - these arrays methods usually change the original array.

// let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
// //without method
// array1[array.length] = 'Francisco';
// console.log(array1);


// //push() - allows to add an element at the end of the array
// array1.push('Andres');
// console.log(array1);


// //unshift() - allows to add an element at the beginning of the array
// array1.unshift('Simon');
// console.log(array1);

// //pop() - allows to delete or remove the last item/element at the end of the array.
// array1.pop();
// console.log(array1);

// //pop() is also able to return the item we removed.
// console.log(array1.pop());
// console.log(array1);

// let removedItem = array1.pop();
// console.log(array1);
// console.log(removedItem);

// //.shift() return the item we removed

// let removedItemShift = array1.shift();
// console.log(array1);
// console.log(removedItemShift);

// // Mini activity

// console.log(array1.shift());
// console.log(array1);
// array1.pop();
// console.log(array1);
// array1.unshift('George');
// console.log(array1);
// array1.push('Michael');
// console.log(array1);


// //.sort() - by default, will allow us to short our items in ascending order.
// let numArray = [3, 2, 1 ,6, 7, 8];
// numArray.sort();
// console.log(numArray);

// let numArray2 = [32, 400, 450, 2, 9, 5, 50, 90];
// numArray2.sort((a,b)=> a-b);
// console.log(numArray2);

// //ascending sort per number's value
// numArray2.sort(function(a,b){
// 	return a-b;
// })
// console.log(numArray2);

// //descending sort per number's value
// numArray2.sort(function(a,b){
// 	return b-a;
// })
// console.log(numArray2);

// let arrayStr = ['Marie', 'Zen', 'Jamie', 'Elaine'];
// arrayStr.sort(function(a,b){
// 	return b-a
// })
// console.log(arrayStr);


// //reverse() - reversed the order of the items
// arrayStr.sort();
// console.log(arrayStr);
// arrayStr.sort().reverse();
// console.log(arrayStr);

// // splice() - allows us to remove and add elements from a given index

// //syntax: array.splice(startingIndex, numberofItemstobeDeleted, elemetstoAdd)

// let beatles = ['George', 'John', 'Paul', 'Ringo'];
// let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq'];

// lakersPlayers.splice(0, 0, 'Caruso');
// console.log(lakersPlayers);
// lakersPlayers.splice(0,1);
// console.log(lakersPlayers);
// lakersPlayers.splice(0,3);
// console.log(lakersPlayers);
// lakersPlayers.splice(1,1);
// console.log(lakersPlayers);
// lakersPlayers.splice(1,0, 'Gasol', 'Fisher');
// console.log(lakersPlayers);

// non-mutators:
	//methors that will not change the original
	// slice() - allows us to get a protion of the original array and return a new array with the items selected from the original
	// syntax: slice(startIndex, endIndex)

let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];
computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer');
console.log(computerBrands);

let newBrands = computerBrands.slice(1,3);
console.log(computerBrands);
console.log(newBrands);

let fonts = ['Times New Roman', ' Comic Sans MS', 'Impact', 'Monotype Corsiva', 'Arial', 'Arial Black'];
let newFontSet = fonts.slice(1, 4);
console.log(newFontSet);

newFontSet = fonts.slice(2);
console.log(newFontSet);

// mini activity;



let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1'];
let microsoft = videoGame.slice(3);
console.log(microsoft);

let nintendo = videoGame.slice(2,4);
console.log(nintendo);


//.toString() - convert the array into a single value as a string but each item will be separated by a comma.
// syntax: array.toString()

let sentence = ['I', 'like', 'Javascript', '.', 'It', 'is', 'fun', '.'];
console.log(sentence);
// console.log(sentenceString);

//.join() - converts array into a single value as a string but aseparator can be specified.
//syntax: array.join(separator)

let sentence2 = ['My', 'favorite', 'fastfood', 'is', 'Army Navy'];
let sentenceString2 = sentence2.join();
console.log(sentenceString2);


let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];


let martin = charArr.slice(5,11);
name1 = martin.join("");
console.log(name1);

let miguel = charArr.slice(13,19);
name2 = miguel.join("");
console.log(name2);

// OR

let name3 = charArr.slice(5,11).join("");
console.log(name3);
let name4 = charArr.slice(13,19).join("");
console.log(name4);



// .concat() - it combines 2 or more arays without affecting the original
// syntax: array.concat(array1, array2)

let tasksFriday = ['drink HTML', 'eat Javascript'];
let tasksSaturday = ['inahle CSS', 'breath BootStrap'];
let tasksSunday = ['Get Git', 'Be Node'];

let weekendTasks = tasksFriday.concat(tasksSaturday, tasksSunday);
console.log(weekendTasks);

// Accesors
	// Methods that allow us to access our array.
	// indexOf()
	// - finds the index of the given element/item when it is first found from the left.

let batch131 = [
		'Paolo',
		'Jamir',
		'Jed',
		'Ronel',
		'Rom',
		'Jayson'	
];

console.log(batch131.indexOf('Jed'));//2
console.log(batch131.indexOf('Rom'));//4


//lastIndexOf() - finds the index of the given element.item when it is last found from the right

console.log(batch131.lastIndexOf('Jamir'));//1
console.log(batch131.lastIndexOf('Jayson'));//5
console.log(batch131.lastIndexOf('Kim'));


// Mini activity


let carBrands = [
	'BMW',
	'Dodge',
	'Maserati',
	'Porsche',
	'Chevrolet',
	'Ferrari',
	'GMC',
	'Porsche',
	'Mitsubishi',
	'Toyota',
	'Volkwagen',
	'BMW'
]

function indexFinder(brand){
	console.log(carBrands.indexOf(brand));
};

function lastIndexFinder(brand){
	console.log(carBrands.lastIndexOf(brand))
};

indexFinder('BMW');
lastIndexFinder('BMW');

indexFinder('Porsche');
lastIndexFinder('Porsche');


// Iterator methods

	// These methods iterate over the items in an array much like loop
	// However
let avengers = [
	'Hulk',
	'Black Widow',
	'Hawkeye',
	'Spider-man',
	'Iron Man',
	'Captain America'

]

//forEach()
	//similar to for loop but is used on arrays. It will allow us to iterate over each time in an array and even add instruction per iteration.
	//Anonymous function within forEach will be receive each and every item in an array.


avengers.forEach(function(avengers){
	console.log(avengers);
});

console.log(avengers);

let marvelHeroes = [
	'Moon Knight',
	'Jessica Jones',
	'Deadpool',
	'Cyclops',
];


marvelHeroes.forEach(function(hero){
	//iterate over all the items in marvel heroes
	// andlet them join the avengers
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		avengers.push(hero);
	}

});

console.log(avengers);

//map()

let number = [25, 50, 30, 10, 5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number * 5
});
console.log(mappedNumbers);

//every()
	// iterates overall the items and checks if all the elements passes a given condition

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18;
});
console.log(checkAllAdult);

//some()
	//iterate overall the items check if evne at least one of items in the array passes the condition. Same as every, it will return boolean.

let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
})


//filter () - creates a new array that contains elements which passed a given condition

let numbersArr2	= [500, 12, 120, 60, 6, 30];
let divisibleBy5 = numbersArr2.filter(function(number){
	return number % 5 === 0;
});
console.log(divisibleBy5);


console.log(checkForPassing);

//find () - iterate over all items in our array but only returns the iemt that will satisfy the gibe condition
//LOGIN FUNCTIONS

// let registerdUsernames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix','sheWhoCode'];

// let foundUser = registerdUsernames.find(function(username){
// 	console.log(username);
// 	return username === 'sheWhoCode';
// });
// console.log(foundUser);

//include() - returns a boolean true if it finds a matching item in the array

// case - sensitive

let registeredEmails = [
	'johnnyPhoenix1991@gmail.com',
	'michealKing@gmail.com',
	'pedro_himself@yahoo.com',
	'sheJonesSmith@gmail.com',
];

// let doesEmailExist = registeredEmails.includes('michealKing@gmail.com');
// console.log(doesEmailExist);




let registeredUsernames = ['pedro101', 'mikeyTheKing2000', 'superPhoenix','sheWhoCode'];

function foundUserName(username){
	let foundUser = registeredUsernames.find(function(username){
 		console.log(username);
 		return username;
 	});
 }

foundUserName('pedro101');




function findEmail(email){
		let emailExist = registeredEmails.includes(email);
		 	if (emailExist === true){
				alert("Email already exist.");
 			} else {
				alert("Email is available, procced to registration.");
 			} 	
} 

findEmail('aaron@gmail.com');